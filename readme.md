

1. [My Useful Page](MyUsefulPage/index.html)
2. [Barry's Business](barrysbusiness/index.html)*
3. [Space Jam 3](boxmodel_beforecss/index.html)*
4. [Float Columns](float-column-positioning/index.html)
5. [Monopoly](monopoly/index.html)
6. [Mega Dropdown](mega_dropdown/index.html)

\* denotes I only wrote the CSS to modify HTML that I was provided. I did not write the HTML.


[![wakatime](https://wakatime.com/badge/user/1beb5578-2dbc-4574-bdc0-fd327bdb8ba2/project/4e40e869-8881-4568-b3e0-6f78e7088942.svg)](https://wakatime.com/badge/user/1beb5578-2dbc-4574-bdc0-fd327bdb8ba2/project/4e40e869-8881-4568-b3e0-6f78e7088942)
